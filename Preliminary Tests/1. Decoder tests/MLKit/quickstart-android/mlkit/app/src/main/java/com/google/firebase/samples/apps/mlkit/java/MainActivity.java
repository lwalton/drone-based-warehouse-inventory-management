/**
 * Code adapted from Google MLKit Sample by Luke Walton to test decoder speed of MLKit.
 */

package com.google.firebase.samples.apps.mlkit.java;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.samples.apps.mlkit.R;

import java.util.Arrays;
import java.util.List;

/**
 * Activity demonstrating different image detector features with a still image from camera.
 */
@KeepName
public final class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainAcitivity TAG";
    private long startTime, endTime;
    private TextView timeView;
    private long[] diffs;
    private String[] values;
    private FirebaseVisionBarcodeDetectorOptions options;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        diffs = new long[21];
        Arrays.fill(diffs, 0);
        values = new String[21];
        Button b = findViewById(R.id.myButton);
        b.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                scanLoop(0);
            }
        });
        options = new FirebaseVisionBarcodeDetectorOptions.Builder()
                .setBarcodeFormats(
                        FirebaseVisionBarcode.FORMAT_QR_CODE, FirebaseVisionBarcode.FORMAT_UPC_A) //UPC_A or QRCode used for testing respective barcodes
                .build();
        timeView = findViewById(R.id.timeView);

    }

    /**
     * Starts a loop that scans 0-20 barcodes
     *
     * @param i integer between 0-20 for the 20 tests
     */
    public void scanLoop(int i) {
        FirebaseVisionBarcodeDetector detector = FirebaseVision.getInstance()
                .getVisionBarcodeDetector(options);
        startTime = System.currentTimeMillis();
        FirebaseVisionImage fbImage = FirebaseVisionImage.fromBitmap(getBitmap(i));
        decode(fbImage, detector, i);
    }

    /**
     * @param fbImage  Firebase image to be analysed by MLKit
     * @param detector MLKit barcode detector
     * @param i        integer between 0 and 20 for the 20 tests
     */
    private void decode(FirebaseVisionImage fbImage, FirebaseVisionBarcodeDetector detector, int i) {
        final int j = i;
        detector.detectInImage(fbImage).addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {
            @Override
            public void onSuccess(List<FirebaseVisionBarcode> barcodes) {
//                //barcode present
                values[j] = barcodes.get(0).getRawValue();
                //1D barcode final digit is check digit, so we check against first 10 digits
                if ((j < 10) && (values[j].substring(0, 11).equals("1234567890" + j)) || (j >= 10) && (values[j].substring(0, 11).equals("123456789" + j))) {
                    endTime = System.currentTimeMillis();
                    diffs[j] = endTime - startTime;
                    if (j == 19) {
                        printAll();
                    } else {
                        scanLoop(j + 1);
                    }
                } else {
                    Log.e(TAG, "Error - wrong scan, not " + j + ", but it is actually " + values[j]);
                }

                //Code used when no barcode present
//                endTime = System.currentTimeMillis();
//                diffs[j] = endTime - startTime;
//                if (j == 19) {
//                    printAll();
//                } else {
//                    scanLoop(j + 1);
//                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "onFailure");
            }
        });
    }

    /**
     * Prints to the Logcat and device screen the time taken to scan each barcode
     */
    private void printAll() {
        StringBuilder diffsString = new StringBuilder(60);
        for (int i = 0; i < 20; i++) {
            diffsString.append(diffs[i]).append("\n");
        }
        Log.e(TAG, diffsString.toString());
        timeView.setText(diffsString.toString());
    }

    /**
     * @param i integer value between 0 and 20 for the 20 test cases
     * @return Bitmap image corresponding to i
     */
    private Bitmap getBitmap(int i) {
        Bitmap myBitmap = null;
        //This is done using switch statements as if it were to be done dynamically
        //it would be very slow as it would need to use reflection reflection
        //This is a quick and dirty solution
        switch (i) {
            case 0:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.a);
                break;
            case 1:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.b);
                break;
            case 2:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.c);
                break;
            case 3:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.d);
                break;
            case 4:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.e);
                break;
            case 5:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.f);
                break;
            case 6:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.g);
                break;
            case 7:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.h);
                break;
            case 8:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.i);
                break;
            case 9:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.j);
                break;
            case 10:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.k);
                break;
            case 11:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.l);
                break;
            case 12:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.m);
                break;
            case 13:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.n);
                break;
            case 14:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.o);
                break;
            case 15:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.p);
                break;
            case 16:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.q);
                break;
            case 17:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.r);
                break;
            case 18:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.s);
                break;
            case 19:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.t);
                break;
        }
        return myBitmap;
    }
}
