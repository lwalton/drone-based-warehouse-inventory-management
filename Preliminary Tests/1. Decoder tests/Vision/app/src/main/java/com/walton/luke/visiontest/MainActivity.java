package com.walton.luke.visiontest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainAcitivity TAG";
    long startTime, endTime;
    TextView timeView;
    BarcodeDetector detector;

    long[] diffs;
    String[] values;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        timeView = findViewById(R.id.timeView);
        diffs = new long[21];
        values = new String[21];

        //barcode detector setup
        detector = new BarcodeDetector.Builder(getApplicationContext())
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        if (!detector.isOperational()) {
            Log.e(TAG, "Decoder fail");
            return;
        }

        Button btn = findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanLoop();
            }
        });
    }

    private void scanLoop() {
        for (int i = 0; i < 20; i++) {
            startTime = System.currentTimeMillis();
            Bitmap image = getBitmap(i);
            SparseArray<Barcode> barcodes = buildFrame(image);

            if (barcodes.size() == 0) {
                //barcode not present
                endTime = System.currentTimeMillis();
                diffs[i] = endTime - startTime;
            } else {
                //Barcode is present
                values[i] = barcodes.valueAt(0).rawValue;
                String value = values[i].substring(0, 11);
                if ((i < 10) && (values[i].equals("01234567890" + i)) || (i >= 10) && (values[i].equals("0123456789" + i))) {
                    endTime = System.currentTimeMillis();
                    diffs[i] = endTime - startTime;
                } else {
                    Log.e(TAG, "Error - wrong scan, not " + i + ", but it is actually " + values[i]);
                }
            }
        }
        StringBuilder diffsString = new StringBuilder(60);

        for (int i = 0; i < 20; i++) {
            diffsString.append(diffs[i]).append("\n");
        }
        Log.e(TAG, diffsString.toString());
        timeView.setText(diffsString.toString());
    }

    private SparseArray<Barcode> buildFrame(Bitmap image) {
        Frame frame = new Frame.Builder().setBitmap(image).build();
        return detector.detect(frame);
    }

    public Bitmap getBitmap(int i) {
        Bitmap myBitmap = null;
        //This is done using switch statements as the only way for this to be done
        //dynamically is very slow as it uses reflection
        switch (i) {
            case 0:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.a);
                break;
            case 1:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.b);
                break;
            case 2:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.c);
                break;
            case 3:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.d);
                break;
            case 4:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.e);
                break;
            case 5:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.f);
                break;
            case 6:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.g);
                break;
            case 7:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.h);
                break;
            case 8:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.i);
                break;
            case 9:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.j);
                break;
            case 10:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.k);
                break;
            case 11:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.l);
                break;
            case 12:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.m);
                break;
            case 13:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.n);
                break;
            case 14:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.o);
                break;
            case 15:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.p);
                break;
            case 16:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.q);
                break;
            case 17:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.r);
                break;
            case 18:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.s);
                break;
            case 19:
                myBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.t);
                break;
        }
        return myBitmap;
    }
}
