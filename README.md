
# Drone Based Warehouse Inventory Management

Code and resources created for the final year project "Drone Based Warehouse Inventory Management" by Luke Walton, 867775@swansea.ac.uk.

Code repos and pictures from preliminary testing are inlcuded in their respecitve folders within the Preliminary Tests folder.

As well as the code used on the Zebra device and pilots device, videos of the tests can be found within the Test folder.

I don't recommend pulling this repository as it contains multiple applications worth of code, as well as videos, pictures and other resources from the comprehensive testing that took place for this project. As such, this repository is very large, over 2GB, so I would not recommend pulling the entire repository.
