# Drone Scan App
# Luke Walton / ripped from Zebra 

## Description

This app is for Zebra devices that are attatched to the top of a drone. Tested on Zebra TC20 running Android 7.1.2 with keyboard, backplane and structual integrity removed.  

## Datawedge files

Datawedge configuration file: [dwprofile_MyScannerHuman.db] [Datawedge files\dwprofile_MyScannerHuman.db]

Profiles needed:
Drone scan profile: [datawedge.db] [Datawedge files\datawedge.db]
Human scan profile: [dwprofile_MyScannerHuman.db] [Datawedge files\dwprofile_MyScannerHuman.db]

## Usage

Drone mode keeps the scanner on at all times after the red onscreen button is pressed. This starts the timer that starts the test. 
Human mode turns the scanner off after a barcode is scanned just like a regular scanning app.
To add barcodes to the "To Scan" list, enter the right hand settings (kebab) menu and select "Add new list". from this screen, scan the barcodes that make up the test. Select done when finished.
Tests can only begin by clicking the onscreen button, not the hardware buttons. Once started, any buttons can be used.
When a test is completed, the "To Scan" view becomes green with a check and the results are saved into a file named as the current date / time. This is stored on the device in folder: `sdcard/DroneScanApp/tests`

If a test is not completed, but you still want to save the results, select "Export last results" from the settings menu.

The scanner can be remotely controlled using the Firestore database which is connected.