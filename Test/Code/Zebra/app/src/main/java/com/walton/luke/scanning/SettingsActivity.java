package com.walton.luke.scanning;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;

import static android.provider.ContactsContract.Intents.Insert.ACTION;

public class SettingsActivity extends AppCompatActivity {

    private static final String PROFILE_NAME = "MyScanner";
    private static final String APP_INTENT_ACTION = "com.walton.luke.myScanner";
    final String TAG = "settings";
    /////////////////////////////////////////////////////////////////////////////////////////////////////

    // DataWedge Extras
    private static final String EXTRA_GET_VERSION_INFO = "com.symbol.datawedge.api.GET_VERSION_INFO";
    private static final String EXTRA_CREATE_PROFILE = "com.symbol.datawedge.api.CREATE_PROFILE";
    private static final String EXTRA_KEY_APPLICATION_NAME = "com.symbol.datawedge.api.APPLICATION_NAME";
    private static final String EXTRA_KEY_NOTIFICATION_TYPE = "com.symbol.datawedge.api.NOTIFICATION_TYPE";
    private static final String EXTRA_REGISTER_NOTIFICATION = "com.symbol.datawedge.api.REGISTER_FOR_NOTIFICATION";
    private static final String EXTRA_UNREGISTER_NOTIFICATION = "com.symbol.datawedge.api.UNREGISTER_FOR_NOTIFICATION";
    private static final String EXTRA_SET_CONFIG = "com.symbol.datawedge.api.SET_CONFIG";
    private static final String EXTRA_KEY_VALUE_SCANNER_STATUS = "SCANNER_STATUS";
    private static final String EXTRA_SEND_RESULT = "SEND_RESULT";
    private static final String EXTRA_EMPTY = "";
    private static final String EXTRA_RESULT_GET_VERSION_INFO = "com.symbol.datawedge.api.RESULT_GET_VERSION_INFO";
    private static final String EXTRA_RESULT = "RESULT";
    private static final String EXTRA_RESULT_INFO = "RESULT_INFO";
    private static final String EXTRA_COMMAND = "COMMAND";
    // DataWedge Actions
    private static final String ACTION_DATAWEDGE = "com.symbol.datawedge.api.ACTION";
    private static final String ACTION_RESULT = "com.symbol.datawedge.api.RESULT_ACTION";
    // private variables
    private Boolean bRequestSendResult = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar mTopToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mTopToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mTopToolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);

        // Check selected decoders
        // Use SET_CONFIG: http://techdocs.zebra.com/datawedge/latest/guide/api/setconfig/
        final Button btnSetDecoders = (Button) findViewById(R.id.btnSetDecoders);
        btnSetDecoders.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {


                final CheckBox checkEAN13 = (CheckBox) findViewById(R.id.chkEAN13);
                String EAN13Value = setDecoder(checkEAN13);

                final CheckBox checkQRCode = (CheckBox) findViewById(R.id.chkQR);
                String QRValue = setDecoder(checkQRCode);

                // Main bundle properties
                Bundle profileConfig = new Bundle();
                profileConfig.putString("PROFILE_NAME", PROFILE_NAME);
                profileConfig.putString("PROFILE_ENABLED", "true");
                profileConfig.putString("CONFIG_MODE", "UPDATE");  // Update specified settingsActivity in profile

                // PLUGIN_CONFIG bundle properties
                Bundle barcodeConfig = new Bundle();
                barcodeConfig.putString("PLUGIN_NAME", "BARCODE");
                barcodeConfig.putString("RESET_CONFIG", "true");

                // PARAM_LIST bundle properties
                Bundle barcodeProps = new Bundle();
                barcodeProps.putString("scanner_selection", "auto");
                barcodeProps.putString("scanner_input_enabled", "true");
                barcodeProps.putString("decoder_ean13", EAN13Value);
                barcodeProps.putString("decoder_qrcode", QRValue);
                barcodeProps.putInt("beam_timer", 0);

                // Bundle "barcodeProps" within bundle "barcodeConfig"
                barcodeConfig.putBundle("PARAM_LIST", barcodeProps);
                // Place "barcodeConfig" bundle within main "profileConfig" bundle
                profileConfig.putBundle("PLUGIN_CONFIG", barcodeConfig);

                // Create APP_LIST bundle to associate app with profile
                Bundle appConfig = new Bundle();
                appConfig.putString("PACKAGE_NAME", getPackageName());
                appConfig.putStringArray("ACTIVITY_LIST", new String[]{"*"});
                profileConfig.putParcelableArray("APP_LIST", new Bundle[]{appConfig});
                sendDataWedgeIntentWithExtra(EXTRA_SET_CONFIG, profileConfig);

                Toast.makeText(getApplicationContext(), "In profile " + PROFILE_NAME + " the selected decoders are being set: " + "\nEAN13=" + EAN13Value + "\nQRCode=" + QRValue, Toast.LENGTH_LONG).show();

            }
        });

        // Register for status change notification
        // Use REGISTER_FOR_NOTIFICATION: http://techdocs.zebra.com/datawedge/latest/guide/api/registerfornotification/
        Bundle b = new Bundle();
        b.putString(EXTRA_KEY_APPLICATION_NAME, getPackageName());
        b.putString(EXTRA_KEY_NOTIFICATION_TYPE, "SCANNER_STATUS");     // register for changes in scanner status
        sendDataWedgeIntentWithExtra(EXTRA_REGISTER_NOTIFICATION, b);

        registerReceivers();

        // Get DataWedge version
        // Use GET_VERSION_INFO: http://techdocs.zebra.com/datawedge/latest/guide/api/getversioninfo/
        sendDataWedgeIntentWithExtra(EXTRA_GET_VERSION_INFO, EXTRA_EMPTY);    // must be called after registering BroadcastReceiver
    }

    // Create profile from UI onClick() event
    public void CreateProfile(View view) {

        // Send DataWedge intent with extra to create profile
        sendDataWedgeIntentWithExtra(EXTRA_CREATE_PROFILE, PROFILE_NAME);

        // Configure created profile to apply to this app
        Bundle profileConfig = new Bundle();
        profileConfig.putString("PROFILE_NAME", PROFILE_NAME);
        profileConfig.putString("PROFILE_ENABLED", "true");
        profileConfig.putString("CONFIG_MODE", "CREATE_IF_NOT_EXIST");  // Create profile if it does not exist

        // Configure barcode input plugin
        Bundle barcodeConfig = new Bundle();
        barcodeConfig.putString("PLUGIN_NAME", "BARCODE");
//        barcodeConfig.putString("RESET_CONFIG", "true"); //  This is the default
        barcodeConfig.putInt("beam_timer", 0);
        profileConfig.putInt("beam_timer", 0);
        Bundle barcodeProps = new Bundle();
        barcodeConfig.putBundle("PARAM_LIST", barcodeProps);
        profileConfig.putBundle("PLUGIN_CONFIG", barcodeConfig);

        // Associate profile with this app
        Bundle appConfig = new Bundle();
        appConfig.putString("PACKAGE_NAME", getPackageName());
        appConfig.putStringArray("ACTIVITY_LIST", new String[]{"*"});
        profileConfig.putParcelableArray("APP_LIST", new Bundle[]{appConfig});
        profileConfig.remove("PLUGIN_CONFIG");

        // Apply configs
        sendDataWedgeIntentWithExtra(EXTRA_SET_CONFIG, profileConfig);

        // Configure intent output for captured data to be sent to this app
        Bundle intentConfig = new Bundle();
        intentConfig.putString("PLUGIN_NAME", "INTENT");
        intentConfig.putString("RESET_CONFIG", "true");
        Bundle intentProps = new Bundle();
        intentProps.putString("intent_output_enabled", "true");
        intentProps.putString("intent_action", APP_INTENT_ACTION);
        intentProps.putString("intent_action", "com.walton.luke.scanning.ACTION");
        intentProps.putString("intent_delivery", "2");
        intentProps.putInt("beam_timer", 0);
        intentConfig.putBundle("PARAM_LIST", intentProps);
        profileConfig.putBundle("PLUGIN_CONFIG", intentConfig);
        sendDataWedgeIntentWithExtra(EXTRA_SET_CONFIG, profileConfig);

        Toast.makeText(this, "Profile created", Toast.LENGTH_LONG).show();

        Log.e(TAG, "profile: " + profileConfig.toString());
        Log.e(TAG, "barcode: " + barcodeConfig.toString());
        Log.e(TAG, "barcode props: " + barcodeProps.toString());
        Log.e(TAG, "intent props: " + intentProps.toString());
    }

    // Create filter for the broadcast intent
    private void registerReceivers() {

        Log.d(TAG, "registerReceivers()");

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_RESULT);                // for error code result
        filter.addCategory(Intent.CATEGORY_DEFAULT);    // needed to get version info

        // register to received broadcasts via DataWedge scanning
        filter.addAction(getResources().getString(R.string.activity_intent_filter_action));
        filter.addAction(getResources().getString(R.string.activity_action_from_service));
        registerReceiver(myBroadcastReceiver, filter);
    }

    // Unregister scanner status notification
    public void unRegisterScannerStatus() {
        Log.d(TAG, "unRegisterScannerStatus()");
        Bundle b = new Bundle();
        b.putString(EXTRA_KEY_APPLICATION_NAME, getPackageName());
        b.putString(EXTRA_KEY_NOTIFICATION_TYPE, EXTRA_KEY_VALUE_SCANNER_STATUS);
        Intent i = new Intent();
        i.setAction(ACTION);
        i.putExtra(EXTRA_UNREGISTER_NOTIFICATION, b);
        this.sendBroadcast(i);
    }

    public String setDecoder(CheckBox decoder) {
        boolean checkValue = decoder.isChecked();
        String value = "false";
        if (checkValue) {
            value = "true";
            return value;
        } else
            return value;
    }

    private BroadcastReceiver myBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Bundle b = intent.getExtras();

            Log.d(TAG, "DataWedge Action:" + action);

            // Get DataWedge version info
            if (intent.hasExtra(EXTRA_RESULT_GET_VERSION_INFO)) {
                Bundle versionInfo = intent.getBundleExtra(EXTRA_RESULT_GET_VERSION_INFO);
                String DWVersion = versionInfo.getString("DATAWEDGE");
                Log.i(TAG, "DataWedge Version: " + DWVersion);
            }

            if (action.equals(getResources().getString(R.string.activity_intent_filter_action))) {
                //  Received a barcode scan
                try {
                    displayScanResult(intent);
                } catch (Exception e) {
                    //  Catch error if the UI does not exist when we receive the broadcast...
                }
            } else if (action.equals(ACTION_RESULT)) {
                // Register to receive the result code
                if ((intent.hasExtra(EXTRA_RESULT)) && (intent.hasExtra(EXTRA_COMMAND))) {
                    String command = intent.getStringExtra(EXTRA_COMMAND);
                    String result = intent.getStringExtra(EXTRA_RESULT);
                    String info = "";

                    if (intent.hasExtra(EXTRA_RESULT_INFO)) {
                        Bundle result_info = intent.getBundleExtra(EXTRA_RESULT_INFO);
                        Set<String> keys = result_info.keySet();
                        for (String key : keys) {
                            Object object = result_info.get(key);
                            if (object instanceof String) {
                                info += key + ": " + object + "\n";
                            } else if (object instanceof String[]) {
                                String[] codes = (String[]) object;
                                for (String code : codes) {
                                    info += key + ": " + code + "\n";
                                }
                            }
                        }
                        Log.d(TAG, "Command: " + command + "\n" +
                                "Result: " + result + "\n" +
                                "Result Info: " + info + "\n");
                        Toast.makeText(getApplicationContext(), "Error Resulted. Command:" + command + "\nResult: " + result + "\nResult Info: " + info, Toast.LENGTH_LONG).show();
                    }
                }

            }
        }
    };

    private void displayScanResult(Intent initiatingIntent) {
        // store decoded data
        String decodedData = initiatingIntent.getStringExtra(getResources().getString(R.string.datawedge_intent_key_data));
        // store decoder type
        String decodedLabelType = initiatingIntent.getStringExtra(getResources().getString(R.string.datawedge_intent_key_label_type));

        final TextView lblScanData = (TextView) findViewById(R.id.lblScanData);
        final TextView lblScanLabelType = (TextView) findViewById(R.id.lblScanDecoder);

        lblScanData.setText(decodedData);
        lblScanLabelType.setText(decodedLabelType);
    }

    private void sendDataWedgeIntentWithExtra(String extraKey, Bundle extras) {
        Intent dwIntent = new Intent();
        dwIntent.setAction(SettingsActivity.ACTION_DATAWEDGE);
        dwIntent.putExtra(extraKey, extras);
        if (bRequestSendResult)
            dwIntent.putExtra(EXTRA_SEND_RESULT, "true");
        Log.e(TAG, dwIntent.getExtras().toString());
        this.sendBroadcast(dwIntent);
    }

    private void sendDataWedgeIntentWithExtra(String extraKey, String extraValue) {
        Intent dwIntent = new Intent();
        dwIntent.setAction(SettingsActivity.ACTION_DATAWEDGE);
        dwIntent.putExtra(extraKey, extraValue);
        if (bRequestSendResult)
            dwIntent.putExtra(EXTRA_SEND_RESULT, "true");
        this.sendBroadcast(dwIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceivers();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(myBroadcastReceiver);
        unRegisterScannerStatus();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
