package com.walton.luke.scanning;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.walton.luke.scanning.DroneModeActivity.ADD_REQUEST_CODE;
import static com.walton.luke.scanning.DroneModeActivity.ScanIntent;
import static com.walton.luke.scanning.DroneModeActivity.buildSaveString;
import static com.walton.luke.scanning.DroneModeActivity.checkPermissions;
import static com.walton.luke.scanning.DroneModeActivity.getBarcodesListFromFile;
import static com.walton.luke.scanning.DroneModeActivity.saveNewFile;

public class HumanModeActivity extends AppCompatActivity {

    private TextView mTextView, mTextView2;
    private ArrayList<String> results;
    private HashMap<String, Pair<String, Boolean>> barcodes;
    private boolean doneFlag, started;
    private long startTime, endTime, lastScanTime = 0;
    private ImageButton mScanButton;
    private FirebaseFirestore db;
    private static final String TAG = "HumanModeActivity";

    /**
     * Adds dropdown menu on click of toolbar menu icon
     *
     * @param menu the menu
     * @return true for success
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_human, menu);
        return true;
    }

    /**
     * Starts activity or function depending on menu item clicked
     *
     * @param item Menu Item that was clicked
     * @return super
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Intent i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                break;
            case R.id.reset:
                initialiseUI();
                initialiseValues();
                break;
            case R.id.addNew:
                Intent j = new Intent(this, AddActivity.class);
                startActivityForResult(j, ADD_REQUEST_CODE);
                break;
            case R.id.drone:
                Intent k = new Intent(this, DroneModeActivity.class);
                startActivity(k);
                break;
            case R.id.export:
                saveNewFile(buildSaveString(getString(R.string.human), startTime, endTime, lastScanTime, barcodes), this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Responds to scan button being pressed
     *
     * @param view View needed as call comes from XML Button. Not used
     */
    public void scanButtonPressed(View view) {
        Log.e(TAG, "scnned button");
        if (doneFlag) {
            initialiseUI();
            started = false;
            doneFlag = false;
            initialiseValues();
            startTime = System.currentTimeMillis();
        }
        if (!started) {
            ScanIntent("START_SCANNING", this);
            if (startTime == 0) {
                startTime = System.currentTimeMillis();
            }
            started = true;
            mScanButton.setImageDrawable(getDrawable(R.drawable.ic_close));
        } else {
            mScanButton.setImageDrawable(getDrawable(R.drawable.ic_barcode));
            started = false;
            ScanIntent("STOP_SCANNING", this);
        }
    }

    /**
     * Runs on creation of activity.
     *
     * @param savedInstanceState UI state bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_human_mode);
        createUI();
        initialiseUI();
        initialiseValues();
    }

    /**
     * Allows scanner to be activated by other device through firebase
     */
    private void firebaseScan() {
        db = FirebaseFirestore.getInstance();
        final DocumentReference docRef = db.collection("scanner").document("V6SpS3MxHsvQRe79RtRP");
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.e(TAG, "Listen failed.", e);
                    return;
                }
                if (snapshot != null && snapshot.exists()) {
                    Boolean onOff = snapshot.getBoolean("onoff");
                    try {
                        if (onOff) {
                            ScanIntent("START_SCANNING", HumanModeActivity.this);
                        } else {
                            ScanIntent("STOP_SCANNING", HumanModeActivity.this);
                        }
                    } catch (NullPointerException e1) {
                        Log.e(TAG, e1.toString());
                    }
                } else {
                    Log.e(TAG, "Current data: null");
                }
            }
        });
    }

    /**
     * Runs onResume of activity
     * Registers broadcast receiver for barcode scanner and initialises values for UI.
     */
    @Override
    protected void onResume() {
        super.onResume();
        registerReceivers();
        initialiseValues();
    }

    /**
     * Runs onPause of activity.
     * Unregisters barcode broadcast receiver
     */
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(humanScanReceiver);
    }

    /**
     * Runs whenever a started activity returns with an intent
     * Used when going between this activity and settings
     *
     * @param requestCode Code of the started activity
     * @param resultCode  Success or failure
     * @param data        Intent containing returned data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                initialiseValues();
            }
        }
    }

    /**
     * Assigns UI components Java values
     */
    private void createUI() {
        mTextView = (TextView) findViewById(R.id.mtextview);
        mTextView2 = (TextView) findViewById(R.id.mtextview2);
        mScanButton = (ImageButton) findViewById(R.id.mScanButton);
        Toolbar mTopToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        mTopToolbar.setTitle(R.string.human_mode);
        setSupportActionBar(mTopToolbar);
    }

    /**
     * Initialises the UI components on the screen
     */
    private void initialiseUI() {

        mTextView.setMovementMethod(new ScrollingMovementMethod());
        mTextView.setText("");
        mTextView.setGravity(Gravity.START);
        mTextView.setTextColor(getColor(R.color.text));
        mTextView.setBackgroundColor(getColor(R.color.white));
        mTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);

        mTextView2.setMovementMethod(new ScrollingMovementMethod());
        mTextView2.setText("");

        mScanButton.setBackgroundColor(getColor(R.color.colorAccent));
        mScanButton.setImageDrawable(getDrawable(R.drawable.ic_barcode));
    }

    /**
     * Initialises all values
     */
    private void initialiseValues() {
        firebaseScan();
        firebaseValues("");
        results = new ArrayList<>();
        barcodes = new HashMap<>();
        startTime = 0;
        endTime = 0;
        lastScanTime = 0;

        if (checkPermissions(this)) {
            Log.e(TAG, "no permissions");
        } else {
            try {
                JSONArray jsonArray = getBarcodesListFromFile();

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    Pair<String, Boolean> p = new Pair<>(object.getString("name"), !object.getBoolean("toScan"));
                    barcodes.put(object.getString("code"), p);
                }
            } catch (JSONException jsonException) {
                Log.e(TAG, "JSON ERROR : " + jsonException.toString());
            } catch (IOException ioException) {
                Log.e(TAG, "JSON ERROR : " + ioException.toString());
            }
        }
        setToScanList();
    }

    /**
     * Sets the text for the To Scan List
     */
    private void setToScanList() {
        String s = "";

        for (Map.Entry<String, Pair<String, Boolean>> entry : barcodes.entrySet()) {
            if (!entry.getValue().second) {
                String name;
                name = entry.getValue().first;
                if (name.equals("")) {
                    name = entry.getKey();
                }
                s = s.concat(name).concat("\n");
            }
        }

        if (s.equals("") && (barcodes.size() > 1) && !doneFlag) {
            mTextView.setGravity(Gravity.CENTER);
            s = "✓";
            mTextView.setTextColor(getColor(R.color.white));
            mTextView.setBackgroundColor(getColor(R.color.green));
            mTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 80f);
            endTime = System.currentTimeMillis();
            ScanIntent("STOP_SCANNING", this);
            saveNewFile(buildSaveString("Human", startTime, endTime, lastScanTime, barcodes), this);
            mScanButton.setImageDrawable(getDrawable(R.drawable.ic_barcode));
            doneFlag = true;
        }
        mTextView.setText(s);
    }

    /**
     * Registers the receivers to get results from the barcode scanner
     */
    private void registerReceivers() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(getResources().getString(R.string.activity_intent_filter_action_2));
        filter.addAction(getResources().getString(R.string.activity_action_from_service));
        registerReceiver(humanScanReceiver, filter);
    }

    /**
     * Adds barcode scan data to lower TextView, showing all scanned barcodes
     *
     * @param i Intent containing scanned barcode information
     */
    private void displayScan(Intent i) {
        String decodedData = i.getStringExtra(getResources().getString(R.string.datawedge_intent_key_data));
        if (results.size() < 1) {
            results.add(decodedData);
        } else if (!decodedData.equals(results.get(results.size() - 1))) {
            results.add(decodedData);
        }
        String s = "";
        for (int j = 0; j < results.size(); j++) {
            s = s.concat(results.get(j));
            s = s.concat("\n");
        }
        mTextView2.setText(s);
        firebaseValues(s);
    }

    /**
     * Sends scanned barcodes to other device via firebase
     * @param values barcodes scanned
     */
    private void firebaseValues(String values) {
        Map<String, Object> scanned = new HashMap<>();
        scanned.put("barcodes", values);
        db.collection("scanner").document("scanned")
                .set(scanned)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e(TAG, "Sent to firebase");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, e.toString());
                    }
                });
    }

    /**
     * Checks if barcode is in list to be scanned, and if so, removes it from the list
     *
     * @param i Intent containing scanned barcode information
     */
    private void checkScanList(Intent i) {
        String decodedData = i.getStringExtra(getResources().getString(R.string.datawedge_intent_key_data));

        if (barcodes.get(decodedData) != null) {
            barcodes.get(decodedData);
            Pair<String, Boolean> p = new Pair<>(barcodes.get(decodedData).first, true);
            barcodes.put(decodedData, p);
            setToScanList();
        }
    }


    /**
     * Broadcast receiver for this class, that receives barcode data, and records the time for testing
     */
    private BroadcastReceiver humanScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            assert action != null;
            if (action.equals(getResources().getString(R.string.activity_intent_filter_action_2))) {
                lastScanTime = System.currentTimeMillis();
                if (doneFlag) scanButtonPressed(null);
                try {
                    checkScanList(intent);
                    displayScan(intent);
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
                started = false;
                mScanButton.setImageDrawable(getDrawable(R.drawable.ic_barcode));
            }
        }
    };
}