package com.walton.luke.scanning;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AddActivity extends AppCompatActivity {

    private static final String TAG = "TAG_addActivity";
    ArrayList<String> results = new ArrayList<>();
    private TextView mTextView;


    /**
     * Starts the barcode scanner
     *
     * @param v View needed as call comes from XML Button. Not used
     */
    public void ToggleScan(View v) {
        DroneModeActivity.ScanIntent("START_SCANNING", this);
    }

    /**
     * Called when user has scanned all new barcodes, to set the values ot be scanned.
     *
     * @param v View needed as call comes from XML Button. Not used
     */
    public void Done(View v) {
        DroneModeActivity.ScanIntent("STOP_SCANNING", this);
        if (results.size() >= 1) {
            moveExistingFile();
            JSONArray jsonArray = createNewFile();
            saveNewFile(jsonArray);
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    /**
     * Runs on creation of activity.
     *
     * @param savedInstanceState UI state bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        initialiseUI();
    }

    /**
     * Called onPause of the activity to unregister the receivers, to prevent them from being in use
     */
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(myBroadcastReceiver);
    }

    /**
     * Called onResume of Activity to register the receivers for the barcode scanner
     */
    @Override
    protected void onResume() {
        super.onResume();
        registerReceivers();
    }

    /**
     * Initialises UI components
     */
    private void initialiseUI() {
        Toolbar mTopToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        ImageButton mScanButton = (ImageButton) findViewById(R.id.mScanButton);
        Button mDoneButton = (Button) findViewById(R.id.mDoneButton);

        setSupportActionBar(mTopToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mTopToolbar.getNavigationIcon().setColorFilter(getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        mTopToolbar.setTitle(R.string.settings);

        mScanButton.setBackgroundColor(getColor(R.color.redAccent));
        mDoneButton.setBackgroundColor(getColor(R.color.green));

        mTextView = (TextView) findViewById(R.id.mtextview);
    }

    /**
     * Registers receivers to get data from barcode scanner
     */
    private void registerReceivers() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(getResources().getString(R.string.activity_intent_filter_action));
        filter.addAction(getResources().getString(R.string.activity_action_from_service));
        registerReceiver(myBroadcastReceiver, filter);
    }

    /**
     * Broadcast receiver definition for getting barcode scan values
     */
    private BroadcastReceiver myBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(getResources().getString(R.string.activity_intent_filter_action))) {
                try {
                    displayScanResult(intent);
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
                ToggleScan(null);
            }
        }
    };

    /**
     * Displays whatever value was lst scanned in the text view
     *
     * @param i intent containing barcode data
     */
    private void displayScanResult(Intent i) {
        String decodedData = i.getStringExtra(getResources().getString(R.string.datawedge_intent_key_data));
        if (results.size() < 1) {
            results.add(decodedData);
        } else if (!results.contains(decodedData)) {
            results.add(decodedData);
        }
        String s = "";
        for (int j = 0; j < results.size(); j++) {
            s = s.concat(results.get(j));
            s = s.concat("\n");
        }
        mTextView.setText(s);
    }

    /**
     * @return current date time in milliseconds
     */
    private static String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd-mm-yy hh-mm-ss");
        return dateFormat.format(new Date());
    }

    /**
     * changes name of existing barcode file to allow for a new one ot be created later on
     */
    private void moveExistingFile() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "no permissions");
        } else {
            File dir = Environment.getExternalStorageDirectory();

            if (dir.exists()) {
                File from = new File(dir, "/DroneScanApp/lists/barcodes.json");
                File to = new File(dir, "/DroneScanApp/lists/" + getDateTime() + ".json");
                if (from.exists()) {
                    from.renameTo(to);
                }
            }
        }
    }

    /**
     * @return JSONArray to be written to file, containing all barcodes to be scanned
     */
    private JSONArray createNewFile() {
        JSONArray jsonArrray = new JSONArray();
        for (int i = 0; i < results.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("name", "");
                jsonObject.put("code", results.get(i));
                jsonObject.put("toScan", true);
                jsonArrray.put(jsonObject);
            } catch (JSONException e) {
                Log.e(TAG, "JSON error : " + e.toString());
            }
        }
        return jsonArrray;
    }

    /**
     * Writes barcodes to be scanned to file
     *
     * @param jsonArray JSONArray containing the details of barcodes to be scanned
     */
    private void saveNewFile(JSONArray jsonArray) {

        try {
            Writer output;
            File file = new File(Environment.getExternalStorageDirectory() + "/DroneScanApp/lists/barcodes.json");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("barcodes", jsonArray);
            output = new BufferedWriter(new FileWriter(file));
            output.write(jsonObject.toString());
            output.close();
            Toast.makeText(getApplicationContext(), "Composition saved", Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            Log.e(TAG, "ERROR - " + e.toString());
            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
