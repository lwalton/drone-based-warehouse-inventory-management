package com.walton.luke.scanning;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    public void startHuman(View view) {
        Intent i = new Intent(this, HumanModeActivity.class);
        startActivity(i);
    }

    public void startDrone(View view) {
        Intent i = new Intent(this, DroneModeActivity.class);
        startActivity(i);
    }
}
