package com.walton.luke.scanning;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DroneModeActivity extends AppCompatActivity {

    private TextView mTextView, mTextView2;
    private ArrayList<String> results;
    private HashMap<String, Pair<String, Boolean>> barcodes;
    private boolean doneFlag, started;
    private long startTime, endTime, lastScanTime = 0;
    private ImageButton mDroneButton;
    private FirebaseFirestore db;
    private static final String TAG = "DroneModeActivity";
    public static final int ADD_REQUEST_CODE = 0;
    public static final String EXTRA_SOFT_SCAN_TRIGGER = "com.symbol.datawedge.api.SOFT_SCAN_TRIGGER";
    public static final String EXTRA_SEND_RESULT = "SEND_RESULT";
    public static final String ACTION_DATAWEDGE = "com.symbol.datawedge.api.ACTION";
    public static Boolean bRequestSendResult = false;

    //todo scan for a long time without stopping

    /**
     * Adds dropdown menu on click of toolbar menu icon
     *
     * @param menu the menu
     * @return true for success
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options, menu);
        return true;
    }

    /**
     * Starts activity or function depending on menu item clicked
     *
     * @param item Menu Item that was clicked
     * @return super
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Intent i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                break;
            case R.id.reset:
                initialiseUI();
                initialiseValues();
                break;
            case R.id.addNew:
                Intent j = new Intent(this, AddActivity.class);
                startActivityForResult(j, ADD_REQUEST_CODE);
                break;
            case R.id.human:
                Intent k = new Intent(this, HumanModeActivity.class);
                startActivity(k);
                break;
            case R.id.export:
                saveNewFile(buildSaveString("Drone", startTime, endTime, lastScanTime, barcodes), this);
                initialiseUI();
                initialiseValues();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Retreives barcodes saved in phone storage
     *
     * @return Barcodes stored in JSON file stored on phone storage
     * @throws JSONException JSON is in wrong format
     * @throws IOException   File not found or file error
     */
    public static JSONArray getBarcodesListFromFile() throws JSONException, IOException {
        FileInputStream inputStream;
        String ret;
        String path = Environment.getExternalStorageDirectory().toString();
        try {
            inputStream = new FileInputStream(path + "/DroneScanApp/lists/barcodes.json");
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String receiveString;
            StringBuilder stringBuilder = new StringBuilder();
            while ((receiveString = bufferedReader.readLine()) != null) {
                stringBuilder.append(receiveString);
            }
            ret = stringBuilder.toString();
            JSONObject jsonObject = new JSONObject(ret);
            return jsonObject.getJSONArray("barcodes");
        } catch (JSONException jsonException) {
            throw jsonException;
        } catch (IOException ioException) {
            throw ioException;
        }
    }

    /**
     * Responds to scan button being pressed
     *
     * @param view View needed as call comes from XML Button. Not used
     */
    public void droneScanButtonPressed(View view) {
        if (doneFlag) {
            initialiseUI();
            started = false;
            doneFlag = false;
            initialiseValues();
            startTime = System.currentTimeMillis();
        }
        if (!started) {
            ScanIntent("START_SCANNING", this);
            if (startTime == 0) {
                startTime = System.currentTimeMillis();
            }
            started = true;
            mDroneButton.setImageDrawable(getDrawable(R.drawable.ic_close));
        } else {
            mDroneButton.setImageDrawable(getDrawable(R.drawable.ic_barcode));
            started = false;
            ScanIntent("STOP_SCANNING", this);
        }
    }

    /**
     * Method for sending an intent to Datawedge
     *
     * @param extraValue value to be put into the intent
     * @param c          context of activity being called from
     */
    public static void ScanIntent(String extraValue, Context c) {
        Intent dwIntent = new Intent();
        dwIntent.setAction(ACTION_DATAWEDGE);
        dwIntent.putExtra(EXTRA_SOFT_SCAN_TRIGGER, extraValue);
        if (bRequestSendResult)
            dwIntent.putExtra(EXTRA_SEND_RESULT, "true");
        c.sendBroadcast(dwIntent);
    }

    /**
     * Checks if the app has permission to read and write to files
     *
     * @param context Context being sent from
     * @return True if permission is allowed, false otherwise
     */
    public static boolean checkPermissions(Context context) {

        return ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Builds a Pair of strings to define the output file of a test
     *
     * @return air containing the name of the new file and the contents of the file
     */
    public static Pair<String, String> buildSaveString(String type, long startTime, long endTime, long lastScanTime, HashMap<String, Pair<String, Boolean>> barcodes) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String d = format.format(endTime) + " " + type;
        String out = type + " scan\n" +
                "Scan started: " + startTime + ", or in regular times: " + format.format(startTime) + "\n" +
                "Final scan at: " + lastScanTime + ", or in regular times: " + format.format(lastScanTime) + "\n" +
                "final scan Time taken: " + (lastScanTime - startTime) + "ms" + "(" + (lastScanTime - startTime) / 1000f + " seconds)\n" +
                "stopped scan at: " + endTime + ", or in regular times: " + format.format(endTime) + "\n" +
                "final scan Time taken: " + (endTime - startTime) + "ms" + " (" + (endTime - startTime) / 1000f + " seconds)\n" +
                barcodes;
        return new Pair<>(out, d);
    }

    /**
     * Saves a log of the test to a file
     *
     * @param outString Pair containing the name of the new file and the contents of the file
     * @param c         Context being sent form
     */
    public static void saveNewFile(Pair<String, String> outString, Context c) {
        try {
            Writer output;
            File file = new File(Environment.getExternalStorageDirectory() + "/DroneScanApp/tests/test_" + outString.second + ".txt");
            output = new BufferedWriter(new FileWriter(file));
            output.write(outString.first);
            output.close();
            Toast.makeText(c.getApplicationContext(), "Scans saved", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Log.e(TAG, "ERROR - " + e.toString());
            Toast.makeText(c.getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Runs on creation of activity.
     *
     * @param savedInstanceState UI state bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drone_mode);
        createUI();
        initialiseUI();
        firebaseScan();
    }

    /**
     * Allows scanner to be activated by other device through firebase
     */
    private void firebaseScan() {
        db = FirebaseFirestore.getInstance();
        final DocumentReference docRef = db.collection("scanner").document("V6SpS3MxHsvQRe79RtRP");
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.e(TAG, "Listen failed.", e);
                    return;
                }
                if (snapshot != null && snapshot.exists()) {
                    Boolean onOff = snapshot.getBoolean("onoff");
                    try {
                        if (onOff) {
                            ScanIntent("START_SCANNING", DroneModeActivity.this);
                        } else {
                            ScanIntent("STOP_SCANNING", DroneModeActivity.this);
                        }
                    } catch (NullPointerException e1) {
                        Log.e(TAG, e1.toString());
                    }
                } else {
                    Log.e(TAG, "Current data: null");
                }
            }
        });
    }

    /**
     * Runs onResume of activity
     * Registers broadcast receiver for barcode scanner and initialises values for UI.
     */
    @Override
    protected void onResume() {
        super.onResume();
        registerReceivers();
        initialiseUI();
        initialiseValues();
    }

    /**
     * Runs onPause of activity.
     * Unregisters barcode broadcast receiver
     */
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(droneScanReceiver);
    }

    /**
     * Runs whenever a started activity returns with an intent
     * Used when going between this activity and settings
     *
     * @param requestCode Code of the started activity
     * @param resultCode  Success or failure
     * @param data        Intent containing returned data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                initialiseValues();
            }
        }
    }

    /**
     * Assigns UI components Java values
     */
    private void createUI() {
        mTextView = (TextView) findViewById(R.id.mtextview);
        mTextView2 = (TextView) findViewById(R.id.mtextview2);
        mDroneButton = (ImageButton) findViewById(R.id.mScanButton);
        Toolbar mTopToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        mTopToolbar.setTitle(R.string.drone_mode);
        setSupportActionBar(mTopToolbar);
    }

    /**
     * Initialises the UI components on the screen
     */
    private void initialiseUI() {
        mDroneButton.setBackgroundColor(getColor(R.color.redAccent));
        mTextView.setMovementMethod(new ScrollingMovementMethod());
        mTextView2.setMovementMethod(new ScrollingMovementMethod());
        mTextView.setGravity(Gravity.START);
        mTextView.setTextColor(getColor(R.color.text));
        mTextView.setBackgroundColor(getColor(R.color.white));
        mTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);
        mDroneButton.setImageDrawable(getDrawable(R.drawable.ic_barcode));
        mTextView.setText("");
        mTextView2.setText("");
    }

    /**
     * Initialises all values
     */
    private void initialiseValues() {
        results = new ArrayList<>();
        barcodes = new HashMap<>();
        startTime = 0;
        endTime = 0;
        lastScanTime = 0;
        if (checkPermissions(this)) {
            Log.e(TAG, "no permissions");
        } else {
            try {
                JSONArray jsonArray = getBarcodesListFromFile();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    Pair<String, Boolean> p = new Pair<>(object.getString("name"), !object.getBoolean("toScan"));
                    barcodes.put(object.getString("code"), p);
                    Log.e(TAG, object.getString("code"));
                }
            } catch (JSONException jsonException) {
                Log.e(TAG, "JSON ERROR : " + jsonException.toString());
            } catch (IOException ioException) {
                Log.e(TAG, "JSON ERROR : " + ioException.toString());
            }
        }
        setToScanList();
    }

    /**
     * Sets the text for the To Scan List
     */
    private void setToScanList() {
        String s = "";

        for (Map.Entry<String, Pair<String, Boolean>> entry : barcodes.entrySet()) {
            if (!entry.getValue().second) {
                String name;
                name = entry.getValue().first;
                if (name.equals("")) {
                    name = entry.getKey();
                }
                s = s.concat(name);
                s = s.concat("\n");
            }
        }

        mTextView.setText(s);

        if (s.equals("") && (barcodes.size() >= 1) && !doneFlag) {
            mTextView.setGravity(Gravity.CENTER);
            mTextView.setText("✓");
            mTextView.setTextColor(getColor(R.color.white));
            mTextView.setBackgroundColor(getColor(R.color.green));
            mTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 80f);
            endTime = System.currentTimeMillis();
            ScanIntent("STOP_SCANNING", this);
            saveNewFile(buildSaveString("Drone", startTime, endTime, lastScanTime, barcodes), this);
            mDroneButton.setImageDrawable(getDrawable(R.drawable.ic_barcode));
            doneFlag = true;
        }
    }

    /**
     * Registers the receivers to get results from the barcode scanner
     */
    private void registerReceivers() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(getResources().getString(R.string.activity_intent_filter_action));
        filter.addAction(getResources().getString(R.string.activity_action_from_service));
        registerReceiver(droneScanReceiver, filter);
    }

    /**
     * Broadcast receiver for this class, that receives barcode data, and records the time for testing
     */
    private BroadcastReceiver droneScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            assert action != null;
            if (action.equals(getResources().getString(R.string.activity_intent_filter_action))) {
                lastScanTime = System.currentTimeMillis();
                if (doneFlag) droneScanButtonPressed(null);
                try {
                    checkScanList(intent);
                    displayScan(intent);
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
                if (!doneFlag) {
                    ScanIntent("START_SCANNING", context);
                }
            }
        }
    };

    /**
     * Adds barcode scan data to lower TextView, showing all scanned barcodes
     *
     * @param i Intent containing scanned barcode information
     */
    private void displayScan(Intent i) {
        String decodedData = i.getStringExtra(getResources().getString(R.string.datawedge_intent_key_data));
        if (results.size() < 1) {
            results.add(decodedData);
        } else if (!decodedData.equals(results.get(results.size() - 1))) {
            results.add(decodedData);
        }
        String s = "";
        for (int j = 0; j < results.size(); j++) {
            s = s.concat(results.get(j));
            s = s.concat("\n");
        }
        firebaseValues(s);
        mTextView2.setText(s);
    }

    /**
     * Sends scanned barcodes to other device via firebase
     * @param values barcodes scanned
     */
    private void firebaseValues(String values){
        Map<String, Object> scanned = new HashMap<>();
        scanned.put("barcodes", values);
        db.collection("scanner").document("scanned")
                .set(scanned)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e(TAG, "Sent to firebase");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, e.toString());
                    }
                });
    }

    /**
     * Checks if barcode is in list to be scanned, and if so, removes it from the list
     *
     * @param i Intent containing scanned barcode information
     */
    private void checkScanList(Intent i) {
        String decodedData = i.getStringExtra(getResources().getString(R.string.datawedge_intent_key_data));

        if (barcodes.get(decodedData) != null) {
            barcodes.get(decodedData);
            Pair<String, Boolean> p = new Pair<>(barcodes.get(decodedData).first, true);
            barcodes.put(decodedData, p);
            setToScanList();
        }
    }
}