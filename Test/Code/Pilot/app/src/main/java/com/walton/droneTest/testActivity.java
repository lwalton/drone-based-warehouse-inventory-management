package com.walton.droneTest;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.HashMap;
import java.util.Map;

/**
 * Activity for demonstrating the functionality of the barcode scanner by Luke Walton, 867775
 */
public class testActivity extends AppCompatActivity {
    private FirebaseFirestore db;
    private boolean scanOnOff;
    private boolean firstLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        //Connect to firebase
        db = FirebaseFirestore.getInstance();
        firstLoad = true;

        final DocumentReference docRef = db.collection("scanner").document("scanned");
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            TextView barcodesBox = (TextView) findViewById(R.id.barcodes);

            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    barcodesBox.setText("null");
                    return;
                }

                if (snapshot != null && snapshot.exists()) {
                    if (firstLoad) {
                        clearFirebase();
                    } else {
                        barcodesBox.setText(snapshot.getString("barcodes"));
                    }
                } else {
                    barcodesBox.setText("Currently: null");

                }
            }
        });
    }

    /**
     * Clears firebase database for scanning
     */
    private void clearFirebase() {
        firstLoad = false;
        Map<String, Object> values = new HashMap<>();
        values.put("barcodes", "");

        db.collection("scanner").document("scanned")
                .set(values)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });
    }

    /**
     * Toggles barcode scanner
     *
     * @param view
     */
    public void scan(View view) {
        DocumentReference docRef = db.collection("scanner").document("V6SpS3MxHsvQRe79RtRP");

        Map<String, Boolean> fire = new HashMap<>();
        fire.put("onoff", scanOnOff);

        docRef.set(fire).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getApplicationContext(), "Scan sent", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "FAIL", Toast.LENGTH_SHORT).show();
            }
        });
        scanOnOff = !scanOnOff;
    }
}
