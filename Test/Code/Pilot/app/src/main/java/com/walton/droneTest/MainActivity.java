package com.walton.droneTest;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.parrot.arsdk.ARSDK;
import com.parrot.arsdk.ardiscovery.ARDISCOVERY_PRODUCT_ENUM;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;
import com.parrot.arsdk.ardiscovery.ARDiscoveryService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.parrot.arsdk.ardiscovery.ARDISCOVERY_PRODUCT_ENUM.ARDISCOVERY_PRODUCT_BEBOP_2;

/**
 * Main Acitivity by Luke walton using Parrot ARDroneSDK3.
 * Lists available drones and toggles barcode scanner
 */
public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_DEVICE_SERVICE = "EXTRA_DEVICE_SERVICE";
    private static final String TAG = "Main Activity";

    //List of runtime permissions
    private static final String[] PERMISSIONS_NEEDED = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
    };

    //permission request result handling
    private static final int REQUEST_CODE_PERMISSIONS_REQUEST = 1;

    //Drone requirements
    public DroneDiscoverer mDroneDiscoverer;
    private final List<ARDiscoveryDeviceService> mDronesList = new ArrayList<>();

    //database stuff
    private FirebaseFirestore db;
    private boolean scanOnOff;


    //load the native libraries
    static {
        ARSDK.loadSDKLibs();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ListView listView = (ListView) findViewById(R.id.list);
        db = FirebaseFirestore.getInstance();

        // Assign adapter to ListView
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ARDiscoveryDeviceService service = (ARDiscoveryDeviceService) mAdapter.getItem(position);
                ARDISCOVERY_PRODUCT_ENUM product = ARDiscoveryService.getProductFromProductID(service.getProductID());
                if (product == ARDISCOVERY_PRODUCT_BEBOP_2) {
                    Intent intent = new Intent(MainActivity.this, BebopActivity.class);
                    intent.putExtra(EXTRA_DEVICE_SERVICE, service);
                    startActivity(intent);
                }
            }
        });

        //Set up SDK
        mDroneDiscoverer = new DroneDiscoverer(this);

        //Set up permissions
        Set<String> permissionsToRequest = new HashSet<>();
        for (String permission : PERMISSIONS_NEEDED) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                    Toast.makeText(this, "Please allow permission " + permission, Toast.LENGTH_LONG).show();
                    finish();
                    return;
                } else {
                    permissionsToRequest.add(permission);
                }
            }
        }
        if (permissionsToRequest.size() > 0) {
            ActivityCompat.requestPermissions(this,
                    permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                    REQUEST_CODE_PERMISSIONS_REQUEST);
        }

        //Set up Firebase document
        final DocumentReference docRef = db.collection("scanner").document("scanned");
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            TextView barcodesBox = (TextView) findViewById(R.id.barcodes);

            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    barcodesBox.setText("null");
                    return;
                }

                if (snapshot != null && snapshot.exists()) {
                    barcodesBox.setText(snapshot.getString("barcodes"));
                } else {
                    barcodesBox.setText("Currently: null");

                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Start doing things
        mDroneDiscoverer.setup();
        mDroneDiscoverer.addListener(mDiscovererListener);
        mDroneDiscoverer.startDiscovering();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Stop doing things
        mDroneDiscoverer.stopDiscovering();
        mDroneDiscoverer.cleanup();
        mDroneDiscoverer.removeListener(mDiscovererListener);
    }

    /**
     * checks permissions needed for drone flight
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        boolean denied = false;
        if (permissions.length == 0) {
            // canceled, finish
            denied = true;
        } else {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    denied = true;
                }
            }
        }

        if (denied) {
            Toast.makeText(this, "soemtthings wrong", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    /**
     * Listener for drone
     */
    private final DroneDiscoverer.Listener mDiscovererListener = new DroneDiscoverer.Listener() {

        @Override
        public void onDronesListUpdated(List<ARDiscoveryDeviceService> dronesList) {
            mDronesList.clear();
            mDronesList.addAll(dronesList);
            mAdapter.notifyDataSetChanged();
        }
    };

    /**
     * Triggers barcode scanner through firebase
     * @param view
     */
    public void barcodeTrigger(View view) {
        DocumentReference docRef = db.collection("scanner").document("V6SpS3MxHsvQRe79RtRP");

        Map<String, Boolean> fire = new HashMap<>();
        fire.put("onoff", scanOnOff);

        docRef.set(fire).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "DocumentSnapshot successfully written!");
                //todo confirm
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //todo error
            }
        });
        scanOnOff = !scanOnOff;
    }

    /**
     * Loads testActivity
     * @param view
     */
    public void tester(View view) {
        startActivity(new Intent(this, testActivity.class));
    }

    static class ViewHolder {
        public TextView text;
    }

    private final BaseAdapter mAdapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return mDronesList.size();
        }

        @Override
        public Object getItem(int position) {
            return mDronesList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = convertView;
            // reuse views
            if (rowView == null) {
                LayoutInflater inflater = getLayoutInflater();
                rowView = inflater.inflate(android.R.layout.simple_list_item_1, null);
                // configure view holder
                ViewHolder viewHolder = new ViewHolder();
                viewHolder.text = (TextView) rowView.findViewById(android.R.id.text1);
                rowView.setTag(viewHolder);
            }

            // fill data
            ViewHolder holder = (ViewHolder) rowView.getTag();
            ARDiscoveryDeviceService service = (ARDiscoveryDeviceService) getItem(position);
            holder.text.setText(service.getName() + " on " + service.getNetworkType());

            return rowView;
        }
    };
}
